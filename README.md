pgbadger
=========

pgbadger role allows you to install,configure and manage pgbadger reports in order to analyze your postgresql database environments


Installation
------------

To install pgbadger ansible role run;

```bash

ansible-galaxy install -r requirements.yml

```

Example Playbook
----------------

```yml
---
- hosts: host
  become: true
  roles:
    - pgbadger

```


PostgreSQL Parameters for pgbadger
------------------

```bash
logging_collector = on
log_filename = 'postgresql-%Y-%m-%d_%H%M%S.log'
log_rotation_age = 10min
log_checkpoints = on
log_connections = on
log_disconnections = on
log_duration = on
log_line_prefix = '%t [%p]: user=%u,db=%d,app=%a,client=%h '
log_lock_waits = on
log_autovacuum_min_duration = 0

```



Author Information
------------------

- Hüseyin DEMİR
